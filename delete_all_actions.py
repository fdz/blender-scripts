import bpy

if bpy.context.object and bpy.context.object.animation_data:
    bpy.ops.object.mode_set(mode='OBJECT')

for action in bpy.data.actions:
    bpy.data.actions.remove(action)
