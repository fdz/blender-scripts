"""
Export each action to a separate .fbx

A custom property can be defined on the armature to define where the animations will be exported
The path should be relative to the file
If no property is defined the animations are exported to where the file is

All nla tracks are removed, they could be muted, but i don't need them right now

This previously depended on https://github.com/A-Ribeiro/CustomBlenderFBXExporter
but, I was having problems with unity fbx importer, it couldn't import custom properties :(
export preset location: AppData\Roaming\Blender Foundation\Blender\3.1\scripts\presets\operator\export_scene.aribeirofbx

TODO:
- [x] don't export CP_PATH
- [ ] it would be cool to only export edited actions to save time
    - other option is to at least export only the current animation
- [ ] right now i'm only exporting the armature, i would like to export the mesh sometimes, but, i'm not modeling right now, so it's not a priority
- [ ] load export settings from export operator presets
"""

import bpy
import os
import json

# CPN = Custom Property name
CPN_PATH = "fdz_export_path"
CPN_HOLDER = "fdz_cp_holder"
CPN_POSE_MARKERS = "pose_markers"

JSON_SEPRATORS = (",",":")

print("\n===================================")
print("START\n")

basedir = os.path.dirname(bpy.data.filepath)
if not basedir:
    raise Exception("Blend file is not saved")

# store active object and selection
view_layer = bpy.context.view_layer
previous_mode = bpy.context.object.mode
obj_active = view_layer.objects.active
selection = bpy.context.selected_objects

bpy.ops.object.mode_set(mode='OBJECT')
bpy.ops.object.select_all(action='DESELECT')

ex = None
try:
    ## this is for exporting what is selected
    #Aprint("objects selected: " + str(len(selection)))
    #for obj in selection:
    armatures = [obj for obj in bpy.data.objects if obj.type == 'ARMATURE']
    for obj in armatures:
        print("OBJECT: " + obj.name)

        # get the bone where custom properties will be saved,
        # this is because when using 'aribeirofbx' exporter with the option 'remove_ghost_armature_root_node_from_export'
        # even though the custom properties of the armature object are being exported
        # unity does not import them, probably because the custom properties of the armature object are bind to the 'ghost armature bone'
        if not CPN_HOLDER in obj:
            raise "CPN_HOLDER missing custom property"
        cp_holder = None
        try:
            cp_holder = obj.pose.bones[obj[CPN_HOLDER]]
        except Exception as e:
            raise "CPN_HOLDER no bone with that name"
        print("custom property holder: ", cp_holder, "\n")
        
        # find custom property that defines relative path to where the animations will be exported
        basedir = os.path.dirname(bpy.data.filepath)
        if CPN_PATH in obj:
            basedir = os.path.abspath(os.path.join(basedir, obj[CPN_PATH]))
        if os.path.exists(basedir) == False:
            raise Exception("path doesn't exists", basedir)

        obj.select_set(True)

        # some exporters only use the active object
        view_layer.objects.active = obj

        # backup obj stuff
        action_backup = obj.animation_data.action
        cp_path_backup = obj[CPN_PATH]
        del obj[CPN_PATH]
        cp_holder_backup = obj[CPN_HOLDER]
        del obj[CPN_HOLDER]
        temp_track = None

        try:
            # export each action as an fbx
            for action in bpy.data.actions:
                print("ACTION: ", action.name)
                
                # remove all nla tracks
                for track in obj.animation_data.nla_tracks:
                    obj.animation_data.nla_tracks.remove(track)

                # pushdown action to new nla track
                #obj.animation_data.action = action
                temp_track = obj.animation_data.nla_tracks.new(prev=None)
                temp_track.name = action.name
                temp_track.strips.new(action.name, 0, action)

                # save pose markers as json in a custom property
                pose_markers = []
                for pose_marker in action.pose_markers:
                    pose_markers.append({ "frame" : pose_marker.frame, "name" : pose_marker.name })
                if len(pose_markers) != 0:
                    pose_markers_json = json.dumps(pose_markers, separators=JSON_SEPRATORS)
                    cp_holder[CPN_POSE_MARKERS] = pose_markers_json
                    print(CPN_POSE_MARKERS, ": ", pose_markers_json)
                elif CPN_POSE_MARKERS in cp_holder:
                    del cp_holder[CPN_POSE_MARKERS]
                
                fn = os.path.join(basedir, bpy.path.clean_name(obj.name)) + "_" + bpy.path.clean_name(action.name)
                # bpy.ops.export_scene.fbx(
                bpy.ops.export_scene.aribeirofbx(
                    remove_ghost_armature_root_node_from_export = True, # aribeirofbx
                    filepath = fn + ".fbx", 
                    use_selection = True,
                    use_active_collection = False,
                    global_scale = 1.0,
                    apply_unit_scale = True,
                    apply_scale_options = 'FBX_SCALE_UNITS',
                    use_space_transform = True,
                    bake_space_transform = True,
                    object_types = {'ARMATURE', 'MESH'},
                    use_mesh_modifiers = True,
                    use_mesh_modifiers_render = True,
                    mesh_smooth_type = 'OFF',
                    use_subsurf = False,
                    use_mesh_edges = False,
                    use_tspace = False,
                    use_custom_props = True,
                    add_leaf_bones = False,
                    primary_bone_axis = 'Y',
                    secondary_bone_axis = 'X',
                    use_armature_deform_only = True,
                    armature_nodetype = 'NULL',
                    bake_anim = True,
                    bake_anim_use_all_bones = True,
                    bake_anim_use_nla_strips = True,
                    bake_anim_use_all_actions = False,
                    bake_anim_force_startend_keying = True,
                    bake_anim_step = 1.0,
                    bake_anim_simplify_factor = 1.0,
                    path_mode = 'AUTO',
                    embed_textures = False,
                    batch_mode = 'OFF',
                    use_batch_own_dir = True,
                    axis_forward = 'Y',
                    axis_up = 'Z',
                )
                print("\n")
        except Exception as e:
            ex = e

        # remove temp stuff
        if temp_track != None:
            obj.animation_data.nla_tracks.remove(temp_track)
        if CPN_POSE_MARKERS in obj:
            del obj[CPN_POSE_MARKERS]
        
        obj.select_set(False)

        # restore obj stuff
        obj.animation_data.action = action_backup
        obj[CPN_PATH] = cp_path_backup
        obj[CPN_HOLDER] = cp_holder_backup
        
        if ex is not None:
            raise ex
        
except Exception as e:
    ex = e

# restore active object and selection
bpy.ops.object.mode_set(mode=previous_mode)
view_layer.objects.active = obj_active
for obj in selection:
    obj.select_set(True)

if ex is not None:
    raise ex

print("END")