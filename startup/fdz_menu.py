import bpy

class FDZMenu(bpy.types.Menu):
    bl_idname = "FDZMenu"
    bl_label = "FDZ"
    
    def draw(self, context):
        layout = self.layout
        layout.operator("export.fdz_actions_current")
        layout.operator("wm.fdz_reload_current_file")

def draw_custom_menu(self, context):
    layout = self.layout
    layout.menu(FDZMenu.bl_idname)

def register():
    bpy.utils.register_class(FDZMenu)
    bpy.types.TOPBAR_MT_editor_menus.append(draw_custom_menu)

def unregister():
    bpy.utils.unregister_class(FDZMenu)
    bpy.types.TOPBAR_MT_editor_menus.remove(draw_custom_menu)

if __name__ == "__main__":
    register()