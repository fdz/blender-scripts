import bpy
import os

class FDZBatchExport(bpy.types.Operator):
    bl_idname = "export.fdz_export_all_actions"
    bl_label = "Export all actions (.fbx)"
    def execute(self, context):
        # find script in one of the scripts directories
        script_filename = "fdz_batch_export.py"
        script_file_path = None
        for path in bpy.utils.script_paths():
            potential_script_path = os.path.join(path, script_filename)
            if os.path.isfile(potential_script_path):
                script_file_path = potential_script_path
                break
        if script_file_path is None:
            return {'CANCELLED'}
        # execute script
        exec(compile(open(script_file_path).read(), script_file_path, 'exec'))
        return {'FINISHED'}

# def menu_func(self, context):
#     self.layout.operator(FDZBatchExport.bl_idname, text="[FDZ] Batch Export (.fbx)")

bpy.utils.register_class(FDZBatchExport)
# bpy.types.TOPBAR_MT_file_export.append(menu_func)