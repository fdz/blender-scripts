# TODO:
# - [?] On the custom properties put the linked libraries to do stuff in unity, like setting the object as humanoid and assigning avatar

import bpy
import os
import json

EXPORT_FILE_NAME = 'settings.json'
JSON_SEPRATORS = (',\n',':')

def settings_find_file(start_directory):
    current_directory = start_directory
    while True:
        settings_path = os.path.join(current_directory, EXPORT_FILE_NAME)
        if os.path.isfile(settings_path):
            return settings_path
        parent_directory = os.path.dirname(current_directory)
        if parent_directory == current_directory:
            break
        current_directory = parent_directory
    return None

def settings_find_file_and_load(start_directory):
    settings_path = settings_find_file(start_directory)
    if settings_path is None:
        return None
    with open(settings_path, 'r') as file:
        settings = json.load(file)
        settings["dir"] = os.path.dirname(settings_path)
        return settings
    return None

def export_fbx(file_path):
    # presets are saves on:
    # WIN: C:\Users\<your username>\AppData\Roaming\Blender Foundation\Blender\<version number>\scripts\presets
    # MAC: Library\Application Support\Blender\<version number>\scripts\presets
    # LINUX: ~/.config/blender/<version number>/scripts/presets/
    # op = bpy.ops.export_scene.fbx
    # armature_node_type = 'NULL'
    op = bpy.ops.export_scene.aribeirofbx
    armature_nodetype = 'REMOVE_GHOST'
    # TODO: export preset per object / armature
    # or at least the settings that i'm changing: apply_scale_options, axis_forward, axis_up
    op(
        filepath = file_path + '.fbx', 
        use_selection = True,
        use_visible = True,
        use_active_collection = False, # False 'cus you can select objects that are not in the active collection
        collection = '',
        global_scale = 1.0,
        apply_unit_scale = True,
        
        # human
        apply_scale_options = 'FBX_SCALE_UNITS',

        # x bot
        # apply_scale_options = 'FBX_SCALE_NONE',

        use_space_transform = True,
        bake_space_transform = True,
        object_types = {'MESH', 'ARMATURE'},
        use_mesh_modifiers = True,
        use_mesh_modifiers_render = True,
        mesh_smooth_type = 'OFF',
        colors_type = 'SRGB',
        prioritize_active_color = False,
        use_subsurf = False,
        use_mesh_edges = False,
        use_tspace = False,
        use_triangles = False,
        use_custom_props = True,
        add_leaf_bones = False,
        primary_bone_axis = 'Y',
        secondary_bone_axis = 'X',
        use_armature_deform_only = True,
        armature_nodetype = armature_nodetype,
        bake_anim = True,
        bake_anim_use_all_bones = True,
        bake_anim_use_nla_strips = True,
        bake_anim_use_all_actions = False,
        bake_anim_force_startend_keying = True,
        bake_anim_step = 1.0,
        bake_anim_simplify_factor = 1.0,
        path_mode = 'AUTO',
        embed_textures = False,
        batch_mode = 'OFF',
        use_batch_own_dir = True,
        
        # human
        axis_forward = 'Y',
        axis_up = 'Z',

        # x bot
        # axis_forward = '-Z',
        # axis_up = 'Y',
    )



def action_to_nla_track(armature, action):
    track = armature.animation_data.nla_tracks.new(prev=None)
    track.name = action.name
    track.strips.new(action.name, 0, action)
    return track

def export_armature_with_action(armature, file_path, action, action_name):
    bpy.ops.object.mode_set(mode='OBJECT')
    bpy.ops.object.select_all(action='DESELECT')
    armature.select_set(True)
    bpy.context.view_layer.objects.active = armature

    K_POSE_MARKERS = 'pose_markers'
    K_ANIM_DATA = 'animations_data'

    prev_action_name = action.name
    action.name = action_name

    for track in armature.animation_data.nla_tracks:
        armature.animation_data.nla_tracks.remove(track)
    nla_track = action_to_nla_track(armature, action)

    def restore():
        action.name = prev_action_name
        if K_ANIM_DATA in first_bone:
            del first_bone[K_ANIM_DATA]
        armature.animation_data.nla_tracks.remove(nla_track)

    try:
        first_bone = armature.pose.bones[0]
        if len(action.pose_markers) != 0:
            # save pose markers as json in a custom property
            print('Serializing pose markers to json')
            animations_data = {}
            anim = animations_data[action_name] = {}
            pose_markers = anim[K_POSE_MARKERS] = []
            for pose_marker in action.pose_markers:
                pose_markers.append({ 'frame': pose_marker.frame, 'name': pose_marker.name })
            action_data_json = json.dumps(animations_data)
            print(action_data_json)
            first_bone[K_ANIM_DATA] = action_data_json
        else:
            print('No pose markers to serialize to json')
            
        export_fbx(file_path)
        
    except Exception as e:
        restore()
        raise e
    
    restore()

class FDZExportCurrentAction(bpy.types.Operator):
    bl_idname = 'export.fdz_actions_current'
    bl_label = 'Export current action (.fbx)'
    bl_description = '''Exports armatures with the active action.
WARNING: This clears the object's NLA Tracks!'''
    
    def execute(self, context):
        # os path checks
        
        blend_file_path = bpy.data.filepath
        blend_file_dir = os.path.dirname(blend_file_path)
        
        if not blend_file_dir:
            self.report({'ERROR'}, 'Blend file is not saved')
            return {'CANCELLED'}

        blend_file_name = os.path.splitext(os.path.basename(bpy.data.filepath))[0] # without extension
        settings = settings_find_file_and_load(blend_file_dir)
        sub = blend_file_dir.replace(settings["dir"], "")
        export_dir = os.path.abspath(os.path.join(settings["dir"], settings["export_dir"])) + sub
        
        if not os.path.isdir(export_dir):
            self.report({'ERROR'}, 'Export dir doesn\'t exists: ' + export_dir)
            return {'CANCELLED'}
        
        # pre export checks
        
        objects = bpy.context.selected_objects
        armatures = [obj for obj in objects if obj.type == 'ARMATURE']
        if len(armatures) == 0:
            self.report({'ERROR'}, 'No armatures selected')
            return {'CANCELLED'}
        
        for armature in armatures:
            if armature.animation_data.action == None:
                self.report({'ERROR'}, 'Armature \"' + armature.name + '\" has no action assigned')
                return {'CANCELLED'}
        
        # start exporting
        
        for armature in armatures:
            action = armature.animation_data.action
            action_name = blend_file_name + '_' + bpy.path.clean_name(action.name)
            export_path = os.path.join(export_dir, action_name)
            self.report({'INFO'}, f'Exporting armature "{armature.name}", action "{action.name}" as "{action_name}", to file "{export_path}"')
            export_armature_with_action(armature, export_path, action, action_name)
        
        self.report({'INFO'}, 'Finished exporting: ' + export_path)
        
        return {'FINISHED'}

def draw_func(self, context):
    layout = self.layout
    layout.operator(FDZExportCurrentAction.bl_idname)

def register():
    bpy.utils.register_class(FDZExportCurrentAction)
    bpy.types.TOPBAR_MT_file_export.append(draw_func)

def unregister():
    bpy.utils.unregister_class(FDZExportCurrentAction)
    bpy.types.TOPBAR_MT_file_export.remove(draw_func)

if __name__ == '__main__':
    register()