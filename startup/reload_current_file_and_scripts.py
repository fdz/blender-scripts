import bpy
import os

class FDZReloadCurrentFile(bpy.types.Operator):
    bl_idname = 'wm.fdz_reload_current_file' # wm = window manager
    bl_label = 'Save & Reload (File & Scripts)'

    def execute(self, context):
        print('\n[FDZ] ' + self.bl_label + '\n')
        # current_mode = context.mode
        # active_object = context.active_object
        bpy.ops.wm.save_mainfile('INVOKE_DEFAULT')
        current_file_path = bpy.data.filepath
        if os.path.isfile(current_file_path):
            bpy.ops.wm.open_mainfile(filepath=current_file_path)
            bpy.ops.script.reload()
            # bpy.context.view_layer.objects.active = active_object
            # bpy.ops.object.mode_set(mode=current_mode)
            self.report({'INFO'}, 'Reloaded')
            return {'FINISHED'}
        else:
            self.report({'ERROR'}, 'No file path found')
            return {'CANCELLED'}

def register():
    bpy.utils.register_class(FDZReloadCurrentFile)

def unregister():
    bpy.utils.unregister_class(FDZReloadCurrentFile)

if __name__ == '__main__':
    register()