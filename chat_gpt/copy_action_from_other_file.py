import bpy

file_path = "//../human_old.blend"
action_name = "melee_whip_att_down_weapon"

with bpy.data.libraries.load(file_path) as (data_from, data_to):
    if action_name in data_from.actions:
        data_to.actions.append(action_name)

if action_name in bpy.data.actions:
    bpy.context.object.animation_data_create()
    bpy.context.object.animation_data.action = bpy.data.actions[action_name]