
import bpy

armatures = [obj for obj in bpy.data.objects if obj.type == 'ARMATURE']
for obj in armatures:
    for track in obj.animation_data.nla_tracks:
        obj.animation_data.nla_tracks.remove(track)