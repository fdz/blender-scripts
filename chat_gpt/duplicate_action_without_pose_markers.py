# by chat gpt

import bpy

def duplicate_action_without_markers(original_action_name):
    # Get the original action
    original_action = bpy.data.actions.get(original_action_name)
    if not original_action:
        print(f"Action '{original_action_name}' not found.")
        return None

    # Create a new action
    new_action = bpy.data.actions.new(name=f"{original_action.name}_copy")

    # Copy F-Curves from the original action
    for fcurve in original_action.fcurves:
        new_fcurve = new_action.fcurves.new(data_path=fcurve.data_path, index=fcurve.array_index)
        new_fcurve.keyframe_points.add(len(fcurve.keyframe_points))
        
        for idx, keyframe in enumerate(fcurve.keyframe_points):
            new_keyframe = new_fcurve.keyframe_points[idx]
            new_keyframe.co = keyframe.co
            new_keyframe.interpolation = keyframe.interpolation
            
            # Copy handle types if necessary
            new_keyframe.handle_left_type = keyframe.handle_left_type
            new_keyframe.handle_right_type = keyframe.handle_right_type
            new_keyframe.handle_left = keyframe.handle_left
            new_keyframe.handle_right = keyframe.handle_right

    # Return the new action
    return new_action

# Example usage
duplicate_action_without_markers("c")