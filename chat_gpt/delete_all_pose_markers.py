import bpy

# Ensure an action is active
if bpy.context.active_object and bpy.context.active_object.animation_data:
    action = bpy.context.active_object.animation_data.action
    if action:
        # Iterate through markers and remove them
        for marker in action.pose_markers[:]:  # Use a copy of the list to avoid modification during iteration
            action.pose_markers.remove(marker)
        print("All markers deleted from the action:", action.name)
    else:
        print("No active action found.")
else:
    print("No active object with animation data found.")
